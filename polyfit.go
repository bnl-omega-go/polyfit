package Polyfit

import (
	"log"
	"math"

	"go-hep.org/x/hep/fit"
	"gonum.org/v1/gonum/optimize"
)

func polynomial(x float64, p []float64) (y float64) {
	y = 0.0
	for i, v := range p {
		y += v * math.Pow(x, float64(i))
	}
	return
}

func gauss(x float64, p []float64) float64 {
	v := (x - p[0]) / p[2]
	return p[1] * math.Exp(-0.5*v*v)
}

type function func(float64, []float64) float64

func funcfit(x, y []float64, deg int64, f function, guess []float64) []float64 {
	fitfunc := fit.Func1D{
		F: func(x float64, ps []float64) float64 {
			return f(x, ps)
		},
		X:  x,
		Y:  y,
		Ps: guess,
	}

	res, err := fit.Curve1D(
		fitfunc,
		nil, &optimize.NelderMead{},
	)

	if err != nil {
		log.Fatal(err)
	}
	if err := res.Status.Err(); err != nil {
		log.Fatal(err)
	}

	return res.X
}

func Polyfit(x, y []float64, deg int64, guess []float64) []float64 {
	return funcfit(x, y, deg, polynomial, guess)
}

func Gaussfit(x, y []float64, deg int64, guess []float64) []float64 {
	return funcfit(x, y, deg, gauss, guess)
}
