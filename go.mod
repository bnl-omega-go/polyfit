module gitlab.cern.ch/bnl-omega-go/polyfit

go 1.17

require (
	go-hep.org/x/hep v0.30.1
	gonum.org/v1/gonum v0.9.3
)

require (
	github.com/gonuts/binary v0.2.0 // indirect
	golang.org/x/exp v0.0.0-20210220032938-85be41e4509f // indirect
	golang.org/x/tools v0.1.8 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
